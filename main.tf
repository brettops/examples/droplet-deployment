resource "tls_private_key" "app" {
  algorithm = "ED25519"
}

resource "digitalocean_ssh_key" "app" {
  name       = "app ssh key"
  public_key = tls_private_key.app.public_key_openssh
}

resource "digitalocean_droplet" "app" {
  image  = "ubuntu-22-04-x64"
  name   = "app"
  region = "sfo3"
  size   = "s-1vcpu-1gb"

  ssh_keys = [digitalocean_ssh_key.app.id]
}
